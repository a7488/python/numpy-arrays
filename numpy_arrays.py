
from matplotlib import pyplot as plt
import random
import timeit
import numpy as np
from scipy import odr
import pylab
import math

#       Одномерный массив
def dim1_np(p):
    A_1 = np.random.sample(p)
    B_1 = np.random.sample(p)
    start_time = timeit.default_timer() 
    A_1*B_1
    time_np = timeit.default_timer() - start_time

    return(time_np)

def dim1(p):
    A_1 = np.random.sample(p)
    B_1 = np.random.sample(p)
    start_time = timeit.default_timer() 
    for i in range(p):
        A_1[i]*B_1[i]
    time = timeit.default_timer() - start_time
    return(time)

p = 1000                                            # количество элементов одномерного массива

x_np = range(1, p, 10)
y_np = []
x = range(1, p, 10)
y = []
for i in range(1, p, 10):
    y_np.append(dim1_np(i))
    y.append(dim1(i))

def f1(B, x):
    return B[0]*x + B[1]
    
linear_model_np = odr.Model(f1)
data_to_fit_np = odr.Data(x_np, y_np)                              # регрессия для np
job_np = odr.ODR(data_to_fit_np, linear_model_np, beta0=[1., 0.])
results_np = job_np.run()
a_fitted_np = results_np.beta[0]
b_fitted_np = results_np.beta[1]
y_pred_np = a_fitted_np * x_np + b_fitted_np

linear_model = odr.Model(f1)
data_to_fit = odr.Data(x, y)                              #регрессия для обычных
job = odr.ODR(data_to_fit, linear_model, beta0=[1., 0.])
results = job.run()
a_fitted = results.beta[0]
b_fitted = results.beta[1]
y_pred = a_fitted * x_np + b_fitted

pylab.subplot (2, 2, 1)
plt.title("dimension = 1")
plt.xlabel("P")
plt.ylabel("Time")
plt.grid()
plt.plot(x, y_pred, linewidth=3)
plt.plot(x_np, y_pred_np,linewidth=3)
plt.legend(['not np', 'np multiplication'])
plt.plot(x, y, 'go', markersize=3)
plt.plot(x_np, y_np, 'ro', markersize=3)
#plt.show()

################################################################################################################################
#       Двумерный массив
def dim2_np(k):
    A_2 = np.random.sample((k,k))
    B_2 = np.random.sample((k,k))
    start_time = timeit.default_timer() 
    A_2*B_2
    time_np = timeit.default_timer() - start_time
    return(time_np)

def dim2(k):
    A_2 = np.random.sample((k,k))
    B_2 = np.random.sample((k,k))
    start_time = timeit.default_timer() 
    for i in range(k):
        for j in range(k):
            A_2[i,j]*B_2[i,j]
    time = timeit.default_timer() - start_time
    return(time)

k = 1000                                       # количество элементов двумерного массива

x_np = range(1, k, 10)
y_np = []
x = range(1, k, 10)
y = []
for i in range(1, k, 10):
    y_np.append(dim2_np(i))
    y.append(dim2(i))

def f2(B, x):
    return B[0]*(x)**2 + B[1]*x + B[2]

quadratic_model_np = odr.Model(f2)
data_to_fit_np = odr.Data(x_np, y_np)                                           # регрессия для np
job_np = odr.ODR(data_to_fit_np, quadratic_model_np, beta0=[2., 1., 0.])
results_np = job_np.run()
a_fitted_np = results_np.beta[0]
b_fitted_np = results_np.beta[1]
c_fitted_np = results_np.beta[2]
y_pred_np = a_fitted_np * x_np * x_np  + b_fitted_np * x_np + c_fitted_np

quadratic_model = odr.Model(f2)
data_to_fit = odr.Data(x, y)                                                       # регрессия для обычных
job = odr.ODR(data_to_fit, quadratic_model, beta0=[2., 1., 0.])
results = job.run()
a_fitted = results.beta[0]
b_fitted = results.beta[1]
c_fitted = results.beta[2]
y_pred = a_fitted * x * x + b_fitted * x + c_fitted

pylab.subplot (2, 2, 2)
plt.title("dimension = 2")
plt.xlabel("P")
plt.ylabel("Time")
plt.grid()
plt.plot(x, y_pred, linewidth=3)
plt.plot(x_np, y_pred_np, linewidth=3)
plt.legend(['not np', 'np multiplication'])
plt.plot(x, y, 'go', markersize=3)
plt.plot(x_np, y_np, 'ro', markersize=3)
#plt.show()

################################################################################################################################
#       Трехмерный массив
def dim3_np(m):
    A_3 = np.random.sample((m,m,m))
    B_3 = np.random.sample((m,m,m))
    start_time = timeit.default_timer() 
    A_3*B_3
    time_np = timeit.default_timer() - start_time
    return(time_np)

def dim3(m):
    A_3 = np.random.sample((m,m,m))
    B_3 = np.random.sample((m,m,m))
    start_time = timeit.default_timer() 
    for i in range(m):
        for j in range(m):
            for k in range(m):
                A_3[i,j,k]*B_3[i,j,k]
    time = timeit.default_timer() - start_time
    return(time)

m = 200                                       # количество элементов трехмерного массива

x_np = range(1, m, 5)
y_np = []
x = range(1, m, 5)
y = []
for i in range(1, m, 5):
    y_np.append(dim3_np(i))
    y.append(dim3(i))

def f3(B, x):
    return B[0]*(x)**3 + B[1]*x**2 + B[2]*x + B[3]

cubic_model_np = odr.Model(f3)
data_to_fit_np = odr.Data(x_np, y_np)                                           # регрессия для np
job_np = odr.ODR(data_to_fit_np, cubic_model_np, beta0=[3., 2., 1., 0.])
results_np = job_np.run()
a_fitted_np = results_np.beta[0]
b_fitted_np = results_np.beta[1]
c_fitted_np = results_np.beta[2]
d_fitted_np = results_np.beta[3]
y_pred_np = a_fitted_np * x_np * x_np * x_np + b_fitted_np * x_np * x_np + c_fitted_np * x_np + d_fitted_np

cubic_model = odr.Model(f3)
data_to_fit = odr.Data(x, y)                                                       # регрессия для обычных
job = odr.ODR(data_to_fit, cubic_model, beta0=[3., 2., 1., 0.])
results = job.run()
a_fitted = results.beta[0]
b_fitted = results.beta[1]
c_fitted = results.beta[2]
d_fitted = results.beta[3]
y_pred = a_fitted * x * x * x + b_fitted * x * x + c_fitted * x + d_fitted

pylab.subplot (2, 2, 3)
plt.title("dimension = 3")
plt.xlabel("P")
plt.ylabel("Time")
plt.grid()
plt.plot(x, y_pred, linewidth=3)
plt.plot(x_np, y_pred_np, linewidth=3)
plt.legend(['not np', 'np multiplication'])
plt.plot(x, y, 'go', markersize=3)
plt.plot(x_np, y_np, 'ro', markersize=3)
plt.show()